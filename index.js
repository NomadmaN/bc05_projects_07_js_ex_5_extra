
// Bài 3: Tính thuế thu nhập cá nhân

// tạo hàm tính thuế thu nhập
function thuNhapChiuThue(thunhap) {
  var thueThuNhap;
  if (thunhap <= 10000000) {
    thueThuNhap = 0;
  } else if (thunhap <= 60000000) {
    thueThuNhap = 0.05;
  } else if (thunhap <= 120000000) {
    thueThuNhap = 0.1;
  } else if (thunhap <= 210000000) {
    thueThuNhap = 0.15;
  } else if (thunhap <= 384) {
    thueThuNhap = 0.2;
  } else if (thunhap <= 624000000) {
    thueThuNhap = 0.25;
  } else if (thunhap <= 960000000) {
    thueThuNhap = 0.3;
  } else {
    thueThuNhap = 0.35;
  }
  return thueThuNhap;
}

// Hàm chính
function btnKetQuaB3() {
  const GIAM_CO_DINH = 4000000;
  var hoTenB3 = document.getElementById("hoTenB3").value;
  var tongThuNhapNam = document.getElementById("tongThuNhapNam").value * 1;
  var soNguoiPhuThuoc = document.getElementById("soNguoiPhuThuoc").value * 1;
  var phanTramThue = thuNhapChiuThue(tongThuNhapNam);

  var tongTienThueThuNhapCaNhan = 0;
  tongTienThueThuNhapCaNhan =
    (tongThuNhapNam - GIAM_CO_DINH - soNguoiPhuThuoc * 1600000) * phanTramThue;
  // kết quả
  document.getElementById(
    "showKetQuaB3"
  ).innerHTML = `Họ tên: ${hoTenB3}; Tiền thuế thu nhập cá nhân: ${tongTienThueThuNhapCaNhan.toLocaleString()} VND`;
  // miễn thuế cho trường hợp thu nhập năm quá thấp
  if (tongThuNhapNam <= 10000000) {
    document.getElementById(
      "showKetQuaB3"
    ).innerHTML = `Thu thuế ông nữa thì ông lấy gì ăn. Miễn thuế nhé!`;
  }
}

// Bài 4: tính tiền cáp
const NHA_DAN = "nhaDan";
const DOANH_NGHIEP = "doanhNghiep";
// tạo hàm tính chi phí
function chiPhiLoaiKhachHang(loaikhach) {
  var chiPhi = 0;
  var phiHoaDonND = 4.5;
  var phiHoaDonDN = 15;
  var phiDVCBND = 20.5;
  var phiDVCBDN = 75;
  var soKenhCC = document.getElementById("soKenhCaoCap").value * 1;
  var phiKenhCCND = 7.5;
  var phiKenhCCDN = 50;
  var soKetNoiDN = document.getElementById("soKetNoi").value * 1;

  if (loaikhach == DOANH_NGHIEP) {
    if (soKetNoiDN <= 10) {
      chiPhi = phiHoaDonDN + phiDVCBDN + (soKenhCC * phiKenhCCDN);
    } else {
      chiPhi =
        phiHoaDonDN + (phiDVCBDN + 5 * (soKetNoiDN - 10)) + (soKenhCC * phiKenhCCDN);
    }
    return chiPhi;
  }
  if (loaikhach == NHA_DAN) {
    chiPhi = phiHoaDonND + phiDVCBND + (soKenhCC * phiKenhCCND);
  } else {
    chiPhi = 0;
  }
  return chiPhi;
}
// ẩn hiện tab kết nối khi chọn loại khách hàng
function chonKH() {
  let loaiKH = document.getElementById("loaiKH").value;
  if (loaiKH == DOANH_NGHIEP) {
    document.getElementById("soKetNoi").style.visibility = "visible";
  } else {
    document.getElementById("soKetNoi").style.visibility = "hidden";
  }
    return loaiKH;
}

// hàm chính
function btnKetQuaB4() {
  var maKH = document.getElementById("maKH").value;
  var loaiKH = document.getElementById("loaiKH").value;
  var tienCapFinal = chiPhiLoaiKhachHang(loaiKH);
  document.getElementById(
    "showKetQuaB4"
  ).innerHTML = `Mã khách hàng: ${maKH}; Tiền cáp: $${tienCapFinal}`;
}
